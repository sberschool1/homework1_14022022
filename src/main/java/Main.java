import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        System.out.println("Введите два числа через пробел и знак + - * или /");
        Scanner in= new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        String symbol = in.next();
        if (symbol.equals("+")){
            System.out.println(a+b);
        }else if(symbol.equals("-")){
            System.out.println(a-b);
        }else if(symbol.equals("*")){
            System.out.println(a*b);
        }else if(symbol.equals("/")){
            if (b!=0){
                System.out.println(a/b);
            }else{
                System.out.println("На ноль делить нельзя");
            }
        }else{
            System.out.println("Введен неверный символ");
        }
    }
}
